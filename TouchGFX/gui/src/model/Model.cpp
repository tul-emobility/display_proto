#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>

extern "C" {
extern osMessageQId gui_queueHandle;
static uint32_t timeoutCounter = 0;
}

Model::Model() :
        modelListener(0) {
}

void Model::tick() {

    /* TODO */
    if (gui_queueHandle != NULL) {
        osEvent event = osMessageGet(gui_queueHandle, 0);
        if (event.status == osEventTimeout) {
            timeoutCounter++;
        }
        else if (event.status == osEventMessage){
            uint16_t value = *(uint16_t*)event.value.p;
        }
    }

}

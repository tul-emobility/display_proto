#include <gui/screen_screen/screenView.hpp>
#include <touchgfx/Color.hpp>
#include "BitmapDatabase.hpp"
#include <texts/TextKeysAndLanguages.hpp>

screenView::screenView() {

}

void screenView::setupScreen() {
/* POWER */
    powerProgress.setXY(405, 400);
    powerProgress.setProgressIndicatorPosition(0, 0, 392, 55);
    powerProgress.setRange(0, 100);
    powerProgress.setDirection(touchgfx::AbstractDirectionProgress::RIGHT);
    powerProgress.setBackground(touchgfx::Bitmap(BITMAP_RIGHTGRAYBACKGROUND_ID));
    powerProgress.setBitmap(BITMAP_RIGHTGREENBACKGROUND_ID);
    powerProgress.setValue(60);
    powerProgress.setAnchorAtZero(true);
    add(powerProgress);

    powerText.setXY(434, 411);
    powerText.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    powerText.setLinespacing(0);
    powerText.setTypedText(touchgfx::TypedText(T_POWER));
    add(powerText);

/* CHARGE */
    chargeProgress.setXY(83, 400);
    chargeProgress.setProgressIndicatorPosition(0, 0, 310, 54);
    chargeProgress.setRange(0, 100);
    chargeProgress.setDirection(touchgfx::AbstractDirectionProgress::LEFT);
    chargeProgress.setBackground(touchgfx::Bitmap(BITMAP_LEFTGRAYBACKGROUND_ID));
    chargeProgress.setBitmap(BITMAP_LEFTGREENBACKGROUND_ID);
    chargeProgress.setValue(60);
    chargeProgress.setAnchorAtZero(true);
    add(chargeProgress);

    chargeText.setXY(238, 411);
    chargeText.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    chargeText.setLinespacing(0);
    chargeText.setTypedText(touchgfx::TypedText(T_CHARGE));
    add(chargeText);


/* BATTERY */
    batteryProgress.setXY(0, 172);
    batteryProgress.setProgressIndicatorPosition(0, 0, 58, 285);
    batteryProgress.setRange(0, 100);
    batteryProgress.setDirection(touchgfx::AbstractDirectionProgress::UP);
    batteryProgress.setBackground(touchgfx::Bitmap(BITMAP_BATTERYGRAY_ID));
    batteryProgress.setBitmap(BITMAP_BATTERY_ID);
    batteryProgress.setValue(60);
    batteryProgress.setAnchorAtZero(true);
    add(batteryProgress);

    line6.setPosition(0, 326, 59, 19);
    line6Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line6.setPainter(line6Painter);
    line6.setStart(0, 0);
    line6.setEnd(57, 0);
    line6.setLineWidth(1);
    line6.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    line8.setPosition(0, 382, 59, 19);
    line8Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line8.setPainter(line8Painter);
    line8.setStart(0, 0);
    line8.setEnd(57, 0);
    line8.setLineWidth(1);
    line8.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    line4.setPosition(0, 268, 59, 19);
    line4Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line4.setPainter(line4Painter);
    line4.setStart(0, 0);
    line4.setEnd(57, 0);
    line4.setLineWidth(1);
    line4.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    line2.setPosition(0, 212, 59, 19);
    line2Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line2.setPainter(line2Painter);
    line2.setStart(0, 0);
    line2.setEnd(57, 0);
    line2.setLineWidth(1);
    line2.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    line3.setPosition(0, 240, 59, 19);
    line3Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line3.setPainter(line3Painter);
    line3.setStart(0, 0);
    line3.setEnd(57, 0);
    line3.setLineWidth(1);
    line3.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    line5.setPosition(0, 296, 59, 19);
    line5Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line5.setPainter(line5Painter);
    line5.setStart(0, 0);
    line5.setEnd(57, 0);
    line5.setLineWidth(1);
    line5.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    line7.setPosition(0, 354, 59, 19);
    line7Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line7.setPainter(line7Painter);
    line7.setStart(0, 0);
    line7.setEnd(57, 0);
    line7.setLineWidth(1);
    line7.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    line9.setPosition(0, 410, 59, 19);
    line9Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line9.setPainter(line9Painter);
    line9.setStart(0, 0);
    line9.setEnd(57, 0);
    line9.setLineWidth(1);
    line9.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    line1.setPosition(0, 184, 59, 19);
    line1Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    line1.setPainter(line1Painter);
    line1.setStart(0, 0);
    line1.setEnd(57, 0);
    line1.setLineWidth(1);
    line1.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

    lightsNormal.setVisible(false);
    lightsNormal.invalidate();

    batteryPercent.setXY(4, 211);
    batteryPercent.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    batteryPercent.setLinespacing(0);
    Unicode::snprintf(percentBuffer, PERCENT_SIZE, "%s", touchgfx::TypedText(T_BATTERYP).getText());
    batteryPercent.setWildcard(percentBuffer);
    batteryPercent.resizeToCurrentText();
    batteryPercent.setTypedText(touchgfx::TypedText(T_BATTERYP));

    range.setXY(5, 271);
    range.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    range.setLinespacing(0);
    range.setTypedText(touchgfx::TypedText(T_RANGEKM));

    rangeUnit.setXY(10, 294);
    rangeUnit.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    rangeUnit.setLinespacing(0);
    rangeUnit.setTypedText(touchgfx::TypedText(T_RANGEUNITTEXT));

    add(line1);
    add(line2);
    add(line3);
    add(line4);
    add(line5);
    add(line6);
    add(line7);
    add(line8);
    add(line9);
    add(rangeUnit);
    add(batteryPercent);
    add(range);
}

void screenView::tearDownScreen() {
    screenViewBase::tearDownScreen();
}

extern "C"
{
    static uint32_t uTick = 0;
}

void screenView::handleTickEvent() {
    uTick++;
//    topIcons(false);
//	tickCounter++;
//	if (tickCounter % 60 == 0) {
//		changeGear(mode);
//		changeLights(lightsMode);
//		topIcons(topMode);
//		runWinkers(winkers);
//		lightsMode++;
//		mode++;
//
//		if (winkers) {
//			winkers = false;
//		} else {
//			winkers = true;
//		}
//
//		if (topMode) {
//			topMode = false;
//		} else {
//			topMode = true;
//		}
//
//		if (mode > 2) {
//			mode = 0;
//		}
//
//		if (lightsMode > 3) {
//			lightsMode = 0;
//		}
//
//	}
//
//	if (tickCounter % 5 == 0) {
//
//		if (value > 100) {
//			direction = false;
//		} else if (value == 0) {
//			direction = true;
//		}
//
//		if (direction) {
//			value++;
//		} else {
//			value--;
//		}
//
//		powerProgress.setValue(value);
//		chargeProgress.setValue(value);
//		batteryProgress.setValue(value);
//		if (value < 15) {
//			chargingGreen.setVisible(false);
//			chargingGreen.invalidate();
//			chargingRed.setVisible(true);
//			chargingRed.invalidate();
//		} else {
//			chargingGreen.setVisible(true);
//			chargingGreen.invalidate();
//			chargingRed.setVisible(false);
//			chargingRed.invalidate();
//
//		}
//	}
}

void screenView::changeLights(int lightsMode) {

    switch (lightsMode) {
    case 0:
        lightsLow.setVisible(true);
        lightsLow.invalidate();
        lightsNormal.setVisible(true);
        lightsNormal.invalidate();
        lightsLongRange.setVisible(true);
        lightsLongRange.invalidate();
        break;

    case 1:
        lightsLow.setVisible(false);
        lightsLow.invalidate();
        break;

    case 2:
        lightsNormal.setVisible(false);
        lightsNormal.invalidate();
        break;

    case 3:
        lightsLongRange.setVisible(false);
        lightsLongRange.invalidate();
        break;

    }
}

void screenView::changeGear(int mode) {
    switch (mode) {
    case 0:
        gearDrive.setColor(touchgfx::Color::getColorFrom24BitRGB(102, 102, 102));
        gearDrive.invalidate();
        gearNeutral.setColor(touchgfx::Color::getColorFrom24BitRGB(49, 219, 40));
        gearNeutral.invalidate();
        break;

    case 1:
        gearNeutral.setColor(touchgfx::Color::getColorFrom24BitRGB(102, 102, 102));
        gearNeutral.invalidate();
        gearReverse.setColor(touchgfx::Color::getColorFrom24BitRGB(49, 219, 40));
        gearReverse.invalidate();
        break;

    case 2:
        gearReverse.setColor(touchgfx::Color::getColorFrom24BitRGB(102, 102, 102));
        gearReverse.invalidate();
        gearDrive.setColor(touchgfx::Color::getColorFrom24BitRGB(49, 219, 40));
        gearDrive.invalidate();
        break;

    }
}

void screenView::topIcons(bool topMode) {
    if (mode) {
        handbrakeFlag.setVisible(true);
        handbrakeFlag.invalidate();
        brakeErrorFlag.setVisible(true);
        brakeErrorFlag.invalidate();
        engineErrorFlag.setVisible(true);
        engineErrorFlag.invalidate();
        warningLightFlag.setVisible(true);
        warningLightFlag.invalidate();
        overheatFlag.setVisible(true);
        overheatFlag.invalidate();
        mainBatteryErrorFlag.setVisible(true);
        mainBatteryErrorFlag.invalidate();
        auxBatteryErrorFlag.setVisible(true);
        auxBatteryErrorFlag.invalidate();
    } else {
        handbrakeFlag.setVisible(false);
        handbrakeFlag.invalidate();
        brakeErrorFlag.setVisible(false);
        brakeErrorFlag.invalidate();
        engineErrorFlag.setVisible(false);
        engineErrorFlag.invalidate();
        warningLightFlag.setVisible(false);
        warningLightFlag.invalidate();
        overheatFlag.setVisible(false);
        overheatFlag.invalidate();
        mainBatteryErrorFlag.setVisible(false);
        mainBatteryErrorFlag.invalidate();
        auxBatteryErrorFlag.setVisible(false);
        auxBatteryErrorFlag.invalidate();
    }
}

void screenView::runWinkers(bool winkers) {
    if (winkers) {
        leftWinker.setVisible(true);
        rightWinker.setVisible(true);
        leftWinker.invalidate();
        rightWinker.invalidate();
    } else {
        leftWinker.setVisible(false);
        rightWinker.setVisible(false);
        leftWinker.invalidate();
        rightWinker.invalidate();
    }

}

void screenView::updateVal(unsigned int variable) {
//	Unicode::snprintf(speedBuffer, SPEED_SIZE,"%d", newValue);
//	speed.resizeToCurrentText();
//	speed.invalidate();

    if (variable / 10 == 1) {
        if (variable - 10 == 0) {
            lightsLow.setVisible(false);
            lightsLow.invalidate();
        } else {
            lightsLow.setVisible(true);
            lightsLow.invalidate();
        }
    }

    if (variable / 10 == 2) {
        if (variable - 20 == 0) {
            lightsNormal.setVisible(false);
            lightsNormal.invalidate();
        } else {
            lightsNormal.setVisible(true);
            lightsNormal.invalidate();
        }
    }

    if (variable / 10 == 3) {
        if (variable - 30 == 0) {
            lightsLongRange.setVisible(false);
            lightsLongRange.invalidate();
        } else {
            lightsLongRange.setVisible(true);
            lightsLongRange.invalidate();
        }
    }

    if (variable / 10 == 4) {
        if (variable - 40 == 0) {
            leftWinker.setVisible(false);
            leftWinker.invalidate();
        } else {
            leftWinker.setVisible(true);
            leftWinker.invalidate();
        }
    }

    if (variable / 10 == 5) {
        if (variable - 50 == 0) {
            rightWinker.setVisible(false);
            rightWinker.invalidate();
        } else {
            rightWinker.setVisible(true);
            rightWinker.invalidate();
        }
    }
//
//
//	if (variable/10 == 6) {
//		if (variable-60 == 0) {
//			handbrake.setVisible(false);
//			handbrake.invalidate();
//		} else {
//			handbrake.setVisible(true);
//			handbrake.invalidate();
//		}
//	}

}

void screenView::updateCAN(unsigned int variable) {

    Unicode::snprintf(percentBuffer, PERCENT_SIZE, "%d", variable);
    batteryPercent.resizeToCurrentText();
    batteryPercent.invalidate();

    batteryProgress.setValue(variable);
    batteryProgress.invalidate();
    if (variable < 15) {
        chargingGreen.setVisible(false);
        chargingGreen.invalidate();
        chargingRed.setVisible(true);
        chargingRed.invalidate();
    } else {
        chargingGreen.setVisible(true);
        chargingGreen.invalidate();
        chargingRed.setVisible(false);
        chargingRed.invalidate();
    }
//		Unicode::snprintf(speedBuffer, SPEED_SIZE,"%d", variable);
//		speed.resizeToCurrentText();
//		speed.invalidate();
}

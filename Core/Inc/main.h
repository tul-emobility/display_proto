/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "cmsis_os.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DISPLAY_MODE_Pin GPIO_PIN_11
#define DISPLAY_MODE_GPIO_Port GPIOD
#define MCU_DI9_Pin GPIO_PIN_11
#define MCU_DI9_GPIO_Port GPIOC
#define MCU_DI10_Pin GPIO_PIN_4
#define MCU_DI10_GPIO_Port GPIOD
#define MCU_DI11_Pin GPIO_PIN_5
#define MCU_DI11_GPIO_Port GPIOD
#define MCU_DI7_Pin GPIO_PIN_9
#define MCU_DI7_GPIO_Port GPIOG
#define MCU_DI8_Pin GPIO_PIN_10
#define MCU_DI8_GPIO_Port GPIOG
#define MCU_DI6_Pin GPIO_PIN_11
#define MCU_DI6_GPIO_Port GPIOG
#define MCU_DI5_Pin GPIO_PIN_13
#define MCU_DI5_GPIO_Port GPIOG
#define MCU_DI1_Pin GPIO_PIN_14
#define MCU_DI1_GPIO_Port GPIOG
#define MCU_DI2_Pin GPIO_PIN_5
#define MCU_DI2_GPIO_Port GPIOB
#define MCU_DI3_Pin GPIO_PIN_7
#define MCU_DI3_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
extern osMessageQId gui_queueHandle;
extern uint32_t os_starttime;
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/*
 * is42s16400j.h
 *
 *  Created on: Jan 12, 2021
 *      Author: zdole
 */

#ifndef INC_IS42S16400J_H_
#define INC_IS42S16400J_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f7xx_hal.h"

#define IS42S16400J_ADDR						((uint32_t)0xd0000000)
#define IS42S16400J_SIZE						((uint32_t)0x00800000)

#define IS42S16400J_REFRESH_COUNT				((uint32_t)1386)
#define IS42S16400J_TIMEOUT						((uint32_t)0xffff)

#define IS42S16400J_BURST_LEN_1					((uint32_t)0x0000)
#define IS42S16400J_BURST_LEN_2					((uint32_t)0x0001)
#define IS42S16400J_BURST_LEN_4					((uint32_t)0x0002)
#define IS42S16400J_BURST_LEN_8					((uint32_t)0x0003)
#define IS42S16400J_BURST_TYPE_SEQUENTIAL		((uint32_t)0x0000)
#define IS42S16400J_BURST_TYPE_INTERLEAVED		((uint32_t)0x0008)
#define IS42S16400J_CAS_LATENCY_2				((uint32_t)0x0020)
#define IS42S16400J_CAS_LATENCY_3				((uint32_t)0x0030)
#define IS42S16400J_OPERATION_STANDARD			((uint32_t)0x0000)
#define IS42S16400J_WRITEBURST_PROGRAMMED		((uint32_t)0x0000)
#define IS42S16400J_WRITEBURST_SINGLE			((uint32_t)0x0200)

HAL_StatusTypeDef is42s16400j_init(SDRAM_HandleTypeDef *sdram, FMC_SDRAM_CommandTypeDef *cmd);

#ifdef __cplusplus
}
#endif

#endif /* INC_IS42S16400J_H_ */

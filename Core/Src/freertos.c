/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "app_touchgfx.h"
#include "database.h"
#include "gpio.h"
#include "iwdg.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
uint32_t os_starttime;
osMessageQId gui_queue;
/* USER CODE END Variables */
osThreadId graphicsTaskHandle;
osThreadId dbSyncTaskHandle;
osThreadId inputPollingTaskHandle;
osThreadId sysUpdateTaskHandle;
osMessageQId db_queueHandle;
osMessageQId gui_queueHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
void dbSyncTaskHandler(void const *arg);
void inputPollingHandler(void const *arg);

void dbReadTestHandler(void const *arg);
void dbUpdateTestHandler(void const *arg);
/* USER CODE END FunctionPrototypes */

void graphicsTaskHandler(void const * argument);
void dbSyncTaskHandler(void const * argument);
void inputPollingHandler(void const * argument);
void sysUpdateTaskHandler(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* GetTimerTaskMemory prototype (linked to static allocation support) */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationIdleHook(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
void configureTimerForRunTimeStats(void) {
    uint32_t timPeriphClock = 2 * HAL_RCC_GetPCLK1Freq();
    uint16_t prescaler = (uint16_t) (timPeriphClock / 100000);
    TIM_HandleTypeDef htim5;
    htim5.Instance = TIM5;
    htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim5.Init.RepetitionCounter = 0;
    htim5.Init.Period = 0xFFFFFFFF;
    htim5.Init.Prescaler = prescaler;

    __TIM5_CLK_ENABLE()
    ;

    HAL_TIM_Base_Init(&htim5);
    HAL_TIM_Base_Start(&htim5);
}

unsigned long getRunTimeCounterValue(void) {
    return (uint32_t) (READ_REG(TIM5->CNT));
}
/* USER CODE END 1 */

/* USER CODE BEGIN 2 */
void vApplicationIdleHook(void) {

}
/* USER CODE END 2 */

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName) {
    /* else wait for watchdog reset or reset instantly */
    while (1) {

    }
#if 0
    __NVIC_SystemReset();
#endif
}
/* USER CODE END 4 */

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer,
        uint32_t *pulIdleTaskStackSize) {
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
    *ppxIdleTaskStackBuffer = &xIdleStack[0];
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/* USER CODE BEGIN GET_TIMER_TASK_MEMORY */
static StaticTask_t xTimerTaskTCBBuffer;
static StackType_t xTimerStack[configTIMER_TASK_STACK_DEPTH];

void vApplicationGetTimerTaskMemory(StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer,
        uint32_t *pulTimerTaskStackSize) {
    *ppxTimerTaskTCBBuffer = &xTimerTaskTCBBuffer;
    *ppxTimerTaskStackBuffer = &xTimerStack[0];
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
/* USER CODE END GET_TIMER_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
    db_init();
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
    /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */

  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
    /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of db_queue */
  osMessageQDef(db_queue, 8, db_queue_message_t);
  db_queueHandle = osMessageCreate(osMessageQ(db_queue), NULL);

  /* definition and creation of gui_queue */
  osMessageQDef(gui_queue, 16, uint16_t);
  gui_queueHandle = osMessageCreate(osMessageQ(gui_queue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
    /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of graphicsTask */
  osThreadDef(graphicsTask, graphicsTaskHandler, osPriorityNormal, 0, 4096);
  graphicsTaskHandle = osThreadCreate(osThread(graphicsTask), NULL);

  /* definition and creation of dbSyncTask */
  osThreadDef(dbSyncTask, dbSyncTaskHandler, osPriorityNormal, 0, 128);
  dbSyncTaskHandle = osThreadCreate(osThread(dbSyncTask), NULL);

  /* definition and creation of inputPollingTask */
  osThreadDef(inputPollingTask, inputPollingHandler, osPriorityNormal, 0, 128);
  inputPollingTaskHandle = osThreadCreate(osThread(inputPollingTask), NULL);

  /* definition and creation of sysUpdateTask */
  osThreadDef(sysUpdateTask, sysUpdateTaskHandler, osPriorityNormal, 0, 512);
  sysUpdateTaskHandle = osThreadCreate(osThread(sysUpdateTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */

  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_graphicsTaskHandler */
/**
 * @brief  Function implementing the graphicsTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_graphicsTaskHandler */
void graphicsTaskHandler(void const * argument)
{
  /* USER CODE BEGIN graphicsTaskHandler */
    /* TouchGFX task */
    MX_TouchGFX_Process();
    /* Infinite loop */
    for (;;) {
        osDelay(10);
    }
  /* USER CODE END graphicsTaskHandler */
}

/* USER CODE BEGIN Header_dbSyncTaskHandler */
/**
 * @brief Function implementing the dbSyncTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_dbSyncTaskHandler */
void dbSyncTaskHandler(void const * argument)
{
  /* USER CODE BEGIN dbSyncTaskHandler */
    /* Infinite loop */
    for (;;) {
        /* Database synchronization task, must run on highest priority */
        db_task();
        osDelay(1);
    }
  /* USER CODE END dbSyncTaskHandler */
}

/* USER CODE BEGIN Header_inputPollingHandler */
static db_block_inputs_t testInputs = { 0 };
/**
 * @brief Function implementing the inputPollingTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_inputPollingHandler */
void inputPollingHandler(void const * argument)
{
  /* USER CODE BEGIN inputPollingHandler */
    /* Infinite loop */
    for (;;) {
        /* Task polling digital inputs and storing values in database */
        testInputs.timestamp = osKernelSysTick();
        testInputs.lights_side = (bool) HAL_GPIO_ReadPin(MCU_DI1_GPIO_Port, MCU_DI1_Pin);
        testInputs.lights_main = (bool) HAL_GPIO_ReadPin(MCU_DI2_GPIO_Port, MCU_DI2_Pin);
        testInputs.lights_longrange = (bool) HAL_GPIO_ReadPin(MCU_DI3_GPIO_Port, MCU_DI3_Pin);
        testInputs.lights_winker_left = (bool) HAL_GPIO_ReadPin(MCU_DI5_GPIO_Port, MCU_DI5_Pin);
        testInputs.lights_winker_right = (bool) HAL_GPIO_ReadPin(MCU_DI6_GPIO_Port, MCU_DI6_Pin);
        testInputs.lights_warning = (bool) HAL_GPIO_ReadPin(MCU_DI7_GPIO_Port, MCU_DI7_Pin);
        testInputs.handbrake = (bool) HAL_GPIO_ReadPin(MCU_DI8_GPIO_Port, MCU_DI8_Pin);
        testInputs.mode_drive = (bool) HAL_GPIO_ReadPin(MCU_DI9_GPIO_Port, MCU_DI9_Pin);
        testInputs.mode_reverse = (bool) HAL_GPIO_ReadPin(MCU_DI10_GPIO_Port, MCU_DI10_Pin);
        testInputs.charging = (bool) HAL_GPIO_ReadPin(MCU_DI11_GPIO_Port, MCU_DI11_Pin);
        db_write_block((void*) &testInputs, DB_BLOCK_INPUT_STATE);
        osDelay(10);
    }
  /* USER CODE END inputPollingHandler */
}

/* USER CODE BEGIN Header_sysUpdateTaskHandler */
static db_block_inputs_t actualInputs = { 0 };
static uint16_t testValue = 0xffff;
/**
 * @brief Function implementing the sysUpdateTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_sysUpdateTaskHandler */
void sysUpdateTaskHandler(void const * argument)
{
  /* USER CODE BEGIN sysUpdateTaskHandler */
    osDelayUntil(&os_starttime, 100);
    /* Infinite loop */
    for (;;) {
        /* Main system task, refresh watchdog and update data on LCD */
        HAL_IWDG_Refresh(&hiwdg);
        uint16_t* pTestValue = &testValue;
        db_read_block((void*) &actualInputs, DB_BLOCK_INPUT_STATE);
        osMessagePut(gui_queueHandle, (uint32_t)pTestValue, 1);
        osDelay(100);
    }
  /* USER CODE END sysUpdateTaskHandler */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

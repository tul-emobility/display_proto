/*
 * is42s16400j.c
 *
 *  Created on: Jan 12, 2021
 *      Author: zdole
 */

#include "fmc.h"
#include "is42s16400j.h"

HAL_StatusTypeDef is42s16400j_init(SDRAM_HandleTypeDef *sdram,
		FMC_SDRAM_CommandTypeDef *cmd) {
	__IO uint32_t tmp = 0;

	cmd->CommandMode = FMC_SDRAM_CMD_CLK_ENABLE;
	cmd->CommandTarget = FMC_SDRAM_CMD_TARGET_BANK2;
	cmd->AutoRefreshNumber = 1;
	cmd->ModeRegisterDefinition = 0;

	HAL_SDRAM_SendCommand(sdram, cmd, 0x1000);

	HAL_Delay(100);

	cmd->CommandMode = FMC_SDRAM_CMD_PALL;
	cmd->CommandTarget = FMC_SDRAM_CMD_TARGET_BANK2;
	cmd->AutoRefreshNumber = 1;
	cmd->ModeRegisterDefinition = 0;

	HAL_SDRAM_SendCommand(sdram, cmd, 0x1000);

	cmd->CommandMode = FMC_SDRAM_CMD_AUTOREFRESH_MODE;
	cmd->CommandTarget = FMC_SDRAM_CMD_TARGET_BANK2;
	cmd->AutoRefreshNumber = 4;
	cmd->ModeRegisterDefinition = 0;

	HAL_SDRAM_SendCommand(sdram, cmd, 0x1000);

	tmp = (uint32_t)IS42S16400J_BURST_LEN_2 			|
					IS42S16400J_BURST_TYPE_INTERLEAVED 	|
					IS42S16400J_CAS_LATENCY_3 			|
					IS42S16400J_OPERATION_STANDARD 		|
					IS42S16400J_WRITEBURST_SINGLE;

	cmd->CommandMode = FMC_SDRAM_CMD_LOAD_MODE;
	cmd->CommandTarget = FMC_SDRAM_CMD_TARGET_BANK2;
	cmd->AutoRefreshNumber = 1;
	cmd->ModeRegisterDefinition = tmp;

	HAL_SDRAM_SendCommand(sdram, cmd, 0x1000);

	HAL_SDRAM_ProgramRefreshRate(sdram, IS42S16400J_REFRESH_COUNT);

	return HAL_OK;
}


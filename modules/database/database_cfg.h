/*
 * database_cfg.h
 *
 *  Created on: Jan 13, 2021
 *      Author: zdole
 */

#ifndef DATABASE_DATABASE_CFG_H_
#define DATABASE_DATABASE_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "stdbool.h"

#define DB_MAX_BLOCK_NUM            2

typedef enum {
    DB_BLOCK_00 = 0,
    DB_BLOCK_01 = 1,
    /* DB_BLOCK_02 = 2,
     DB_BLOCK_03 = 3,
     DB_BLOCK_04 = 4,
     DB_BLOCK_05 = 5,
     DB_BLOCK_06 = 6,
     DB_BLOCK_07 = 7,
     DB_BLOCK_08 = 8,
     DB_BLOCK_09 = 9,
     DB_BLOCK_10 = 10,
     DB_BLOCK_11 = 11,
     DB_BLOCK_12 = 12,
     DB_BLOCK_13 = 13,
     DB_BLOCK_14 = 14,
     DB_BLOCK_15 = 15,
     DB_BLOCK_16 = 16,
     DB_BLOCK_17 = 17,
     DB_BLOCK_18 = 18,
     DB_BLOCK_19 = 19,
     DB_BLOCK_20 = 20,
     DB_BLOCK_21 = 21,
     DB_BLOCK_22 = 22,
     DB_BLOCK_23 = 23,
     DB_BLOCK_24 = 24,*/
    DB_BLOCK_MAX = DB_MAX_BLOCK_NUM,
} db_block_id_t;

typedef enum {
    WRITE_ACCESS = 0, /*!< write access to data block */
    READ_ACCESS = 1, /*!< read access to data block  */
} db_block_acces_type_t;

typedef struct {
    void *blockptr;
    uint16_t datalength;
} db_base_header_t;

typedef struct {
    uint8_t nr_of_blockheader;
    db_base_header_t *blockheaderptr;
} db_base_header_dev_t;

#define DB_BLOCK_ACTUAL_SPEED           DB_BLOCK_00
#define DB_BLOCK_INPUT_STATE            DB_BLOCK_01

typedef struct {
    uint32_t timestamp;
    uint32_t timestamp_prev;
    uint16_t speed;
} db_block_speed_t;

typedef struct {
    uint32_t timestamp;
    uint32_t timestamp_prev;
    bool lights_side;
    bool lights_main;
    bool lights_longrange;
    bool lights_winker_left;
    bool lights_winker_right;
    bool lights_warning;
    bool handbrake;
    bool mode_drive;
    bool mode_reverse;
    bool charging;
} db_block_inputs_t;

extern const db_base_header_dev_t db_base_dev;

#ifdef __cplusplus
}
#endif

#endif /* DATABASE_DATABASE_CFG_H_ */

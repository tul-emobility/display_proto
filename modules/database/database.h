/*
 * database.h
 *
 *  Created on: Jan 13, 2021
 *      Author: zdole
 */

#ifndef DATABASE_DATABASE_H_
#define DATABASE_DATABASE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "cmsis_os.h"

#include "database_cfg.h"

#define DB_QUEUE_TIMEOUT    (20)
#define DB_QUEUE_LEN        (1)

typedef struct {
    void *value;
    db_block_id_t block_id;
    db_block_acces_type_t access_type;
} db_queue_message_t;

typedef struct {
    void *rdptr;
    void *wrptr;
} db_block_access_t;

extern osPoolId db_pool;
extern osMessageQId db_queue;

extern void db_init(void);

extern void db_write_block(void *pdata, db_block_id_t block);

extern osStatus db_read_block(void *pdata, db_block_id_t block);

extern void db_task(void);

#ifdef __cplusplus
}
#endif

#endif /* DATABASE_DATABASE_H_ */

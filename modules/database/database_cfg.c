/*
 * database_cfg.c
 *
 *  Created on: Jan 13, 2021
 *      Author: zdole
 */

#include "database_cfg.h"

static db_block_speed_t db_block_speed;

static db_block_inputs_t db_block_inputs;

static db_base_header_t db_base_header[] = {
        {
                (void*)(&db_block_speed),
                sizeof(db_block_speed_t)
        },
        {
                (void*)(&db_block_inputs),
                sizeof(db_block_inputs)
        }
};

const db_base_header_dev_t db_base_dev = {
        .nr_of_blockheader = sizeof(db_base_header)/sizeof(db_base_header_t),
        .blockheaderptr = &db_base_header[0]
};

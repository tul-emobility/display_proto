/*
 * database.c
 *
 *  Created on: Jan 13, 2021
 *      Author: zdole
 */

#include "database.h"

#include "string.h"

static db_block_access_t db_block_access[DB_MAX_BLOCK_NUM];

osPoolId db_pool;
osPoolDef(db_pool, DB_QUEUE_LEN, db_queue_message_t);

osMessageQId db_queue;
osMessageQDef(db_queue, DB_QUEUE_LEN, db_queue_message_t);

extern void db_init(void) {
    if (sizeof(db_base_dev) == 0) {
        while (1) {

        }
    }
    for (uint16_t i = 0; i < db_base_dev.nr_of_blockheader; i++) {
        db_block_access[i].wrptr = (void*) *(uint32_t*) (db_base_dev.blockheaderptr + i);
        db_block_access[i].rdptr = db_block_access[i].wrptr;
        uint8_t *db_entry_wr = (uint8_t*) db_block_access[i].wrptr;
        uint8_t *db_entry_rd = (uint8_t*) db_block_access[i].rdptr;

        for (uint16_t j = 0; j < (db_base_dev.blockheaderptr + i)->datalength; j++) {
            *db_entry_wr = 0;
            db_entry_wr++;
            *db_entry_rd = 0;
            db_entry_rd++;
        }
    }

    db_pool = osPoolCreate(osPool(db_pool));
    db_queue = osMessageCreate(osMessageQ(db_queue), NULL);

    if (db_queue == NULL) {
        while (1) {

        }
    }
}

extern void db_write_block(void *pdata, db_block_id_t block) {
    db_queue_message_t *write_msg = NULL;
    while (write_msg == NULL) {
        write_msg = osPoolAlloc(db_pool);
    }
    uint32_t q_timeout;

    q_timeout = DB_QUEUE_TIMEOUT / portTICK_RATE_MS;
    if (q_timeout == 0) {
        q_timeout = 1;
    }

    write_msg->block_id = block;
    write_msg->access_type = WRITE_ACCESS;
    write_msg->value = pdata;

    osMessagePut(db_queue, (uint32_t) write_msg, q_timeout);
    osPoolFree(db_pool, write_msg);
}

extern osStatus db_read_block(void *pdata, db_block_id_t block) {
    db_queue_message_t *read_message = NULL;
    while (read_message == NULL) {
        read_message = osPoolAlloc(db_pool);
    }
    uint32_t q_timeout;

    q_timeout = DB_QUEUE_TIMEOUT / portTICK_RATE_MS;
    if (q_timeout == 0) {
        q_timeout = 1;
    }

    read_message->block_id = block;
    read_message->access_type = READ_ACCESS;
    read_message->value = pdata;

    osMessagePut(db_queue, (uint32_t) read_message, q_timeout);
    osPoolFree(db_pool, read_message);
    return osOK;
}

extern void db_task(void) {
    db_queue_message_t *receive_msg;
    osEvent event;
    void *srcdataptr;
    void *dstdataptr;
    db_block_id_t block_id;
    db_block_acces_type_t access_type; /* read or write access type */
    uint16_t datalen;

    if (db_queue != NULL) {
        event = osMessageGet(db_queue, 1);
        if (event.status == osEventMessage) {
            receive_msg = (db_queue_message_t*) event.value.p;
            block_id = receive_msg->block_id;
            srcdataptr = receive_msg->value;
            access_type = receive_msg->access_type;
            if ((block_id < DB_MAX_BLOCK_NUM) && (srcdataptr != NULL)) {
                if (access_type == WRITE_ACCESS) {
                    /* write access to data blocks */
                    datalen = (db_base_dev.blockheaderptr + block_id)->datalength;
                    dstdataptr = db_block_access[block_id].wrptr;

                    uint32_t *ts_prev = NULL;
                    uint32_t *ts_act = NULL;
                    ts_act = (uint32_t*) dstdataptr;
                    ts_prev = (uint32_t*) srcdataptr;
                    ts_prev++;
                    *ts_prev = *ts_act;
                    *(uint32_t*) srcdataptr = osKernelSysTick();

                    memcpy(dstdataptr, srcdataptr, datalen);

                } else if (access_type == READ_ACCESS) {
                    /* Read access to data blocks */
                    datalen = (db_base_dev.blockheaderptr + block_id)->datalength;
                    dstdataptr = srcdataptr;

                    srcdataptr = db_block_access[block_id].rdptr;
                    if (srcdataptr != NULL) {
                        memcpy(dstdataptr, srcdataptr, datalen);
                    }
                }
            }
        }
    }
}

